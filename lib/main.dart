import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterarchitecturedemo/models/movie_response.dart';
import 'package:flutterarchitecturedemo/provider/movie_provider.dart';
import 'package:flutterarchitecturedemo/services/api_response.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => MovieModel()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Network Handling Demo',
          home: MovieScreen(),
        ));
  }
}

class MovieScreen extends StatefulWidget {
  @override
  _MovieScreenState createState() => _MovieScreenState();
}

class _MovieScreenState extends State<MovieScreen> {
  MovieModel _bloc;
 

  @override
  void initState() {
    super.initState();
    _bloc = MovieModel();

  }

  @override
  Widget build(BuildContext context) {
       final     movieProvider = Provider.of<MovieModel>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Movie Mania',
            style: TextStyle(color: Colors.lightGreen, fontSize: 28)),
        backgroundColor: Colors.black54,
      ),
      backgroundColor: Colors.black54,
      body: RefreshIndicator(
        onRefresh: () => movieProvider.fetchMovieList(),
        child: Consumer<MovieModel>(
          builder: (context, movieModel, child) {
          
            print('movie list status : ${movieModel.movieList.status}');
            if (movieModel.movieList.status == Status.LOADING) {
              return Loading(loadingMessage: movieModel.movieList.message);
            } else if (movieModel.movieList.status == Status.COMPLETED) {
              return MovieList(movieList: movieModel.movieList.data);
            } else if (movieModel.movieList.status == Status.ERROR) {
              return Error(
                  errorMessage: movieModel.movieList.message,
                  onRetryPressed: () => movieProvider.fetchMovieList());
            } else {
              return Container();
            }
            // switch (movieModel.movieList.status) {
            //   case Status.LOADING:
            //     print("loading");
            //     return Loading(loadingMessage: movieModel.movieList.message);
            //     break;
            //   case Status.COMPLETED:
            //     print('completed');
            //     return MovieList(movieList: movieModel.movieList.data);
            //     break;
            //   case Status.ERROR:
            //     print('error');
            //     return Error(
            //         errorMessage: movieModel.movieList.message,
            //         onRetryPressed : () => _bloc.fetchMovieList()
            //         );
            //     break;
            // }
            // return Container();
          },
        )
        // FutureBuilder(
        //   future: _movieList,
        // builder: (context, snapshot) {
        //      if (snapshot.hasData) {
        //       switch (snapshot.data.status) {
        //         case Status.LOADING:
        //           return Loading(loadingMessage: snapshot.data.message);
        //           break;
        //         case Status.COMPLETED:
        //           return MovieList(movieList: snapshot.data.data);
        //           break;
        //         case Status.ERROR:
        //           return Error(
        //             errorMessage: snapshot.data.message,
        //             onRetryPressed: ()  {
        //               _movieList = _bloc.fetchMovieList();
        //               setState(() {

        //               });

        //             }
        //           );
        //           break;
        //       }
        //     }
        //     return Container();
        //   }
        // )
        ,
      ),
    );
  }

  @override
  void dispose() {
    // _bloc.dispose();
    super.dispose();
  }
}

class MovieList extends StatelessWidget {
  final List<Movie> movieList;

  const MovieList({Key key, this.movieList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: movieList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1.5 / 1.8,
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Image.network(
                'https://image.tmdb.org/t/p/w342${movieList[index].posterPath}',
                fit: BoxFit.fill,
              ),
            ),
          ),
        );
      },
    );
  }
}

class Error extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const Error({Key key, this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.lightGreen,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          RaisedButton(
            color: Colors.lightGreen,
            child: Text('Retry', style: TextStyle(color: Colors.white)),
            onPressed: onRetryPressed,
          )
        ],
      ),
    );
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.lightGreen,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightGreen),
          ),
        ],
      ),
    );
  }
}
