import 'package:flutter/material.dart';
import 'package:flutterarchitecturedemo/models/movie_response.dart';
import 'package:flutterarchitecturedemo/repository/movie_repository.dart';
import 'package:flutterarchitecturedemo/services/api_response.dart';

class MovieModel extends ChangeNotifier {
  MovieRepository _movieRepository;

  ApiResponse<List<Movie>> _movieList;

  ApiResponse<List<Movie>> get movieList => _movieList;

  MovieModel() {
    _movieList = ApiResponse<List<Movie>>();
    _movieRepository = MovieRepository();
     fetchMovieList();
  }

  fetchMovieList() async {
    _movieList = ApiResponse.loading('Fetching Popular Movies');
    notifyListeners();
    try {
      List<Movie> movies = await _movieRepository.fetchMovieList();
      _movieList = ApiResponse.completed(movies);
      notifyListeners();
      // print('movie list length : ${movieList.data.length}');
      // print('movie list status : ${movieList.status}');
    } catch (e) {
      _movieList = ApiResponse.error(e.toString());
      notifyListeners();
    }
  }
}
